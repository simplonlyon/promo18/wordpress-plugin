<?php
global $title;
?>

<h1><?= $title ?></h1>

<form action="options.php" method="post">
    <?php settings_fields('simplon_group'); ?>
    <?php do_settings_sections('simplon_group'); ?>
    <div>
        <label for="admin_color">Admin color</label>
        <input type="color" name="admin_color" id="admin_color" value="<?= esc_attr(get_option("admin_color")) ?>">
    </div>
    <div>
        <label for="admin_title_size">Admin Title size</label>
        <input type="number" name="admin_title_size" id="admin_title_size" value="<?= esc_attr(get_option("admin_title_size")) ?>" step="0.1">
    </div>
    <?php submit_button(); ?>
</form>