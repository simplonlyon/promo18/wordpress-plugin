<?php
/**
 * Classe représentant un nouveau bloc élementor qu'il faudra charger dans le functions.php
 */
class SimplonElementor extends \Elementor\Widget_Base
{

    public function __construct($data = array(), $args = null)
    {
        parent::__construct($data, $args);

        wp_register_style('simplon-widget-style', plugins_url('/style.css', __FILE__), array(), '1.0.0');
    }

    /**
     * Enqueue styles.
     */
    public function get_style_depends()
    {
        return array('simplon-widget-style');
    }


    
    public function get_name()
    {
        return 'simplon_elementor';
    }
    
    public function get_title()
    {
        return 'Simplon Elementor';
    }
    
    protected function _register_controls()
    {
        $this->start_controls_section('first_section', [
            'label' => 'Configure Widget',
            'tab' => \Elementor\Controls_Manager::TAB_CONTENT
        ]);

        $this->add_control('description', [
            'label' => 'Description',
            'type' => \Elementor\Controls_Manager::TEXTAREA
        ]);

        $this->add_control('image', [
            'label' => 'Image',
            'type' => \Elementor\Controls_Manager::MEDIA
        ]);

        $this->add_control('link', [
            'label' => 'Link to',
            'type' => \Elementor\Controls_Manager::URL
        ]);

        $this->end_controls_section();

        
    }
    
    protected function render()
    {
        $instance = $this->get_settings_for_display();
        
        require 'template.php';
    }
}
