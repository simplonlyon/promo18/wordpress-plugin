<?php

/**
 * Classe représentant un nouveau bloc élementor qu'il faudra charger dans le functions.php
 */
class PromoList extends \Elementor\Widget_Base
{

    public function __construct($data = array(), $args = null)
    {
        parent::__construct($data, $args);

        wp_register_style('promo-widget-style', plugins_url('/style.css', __FILE__), array(), '1.0.0');
    }

    /**
     * Enqueue styles.
     */
    public function get_style_depends()
    {
        return array('promo-widget-style');
    }



    public function get_name()
    {
        return 'promo_list';
    }

    public function get_title()
    {
        return 'Promo List';
    }

    protected function _register_controls()
    {
        $this->start_controls_section('promo_display', [
            'label' => 'Display type',
            'tab' => \Elementor\Controls_Manager::TAB_CONTENT
        ]);

        $this->add_control('is_flex', [
            'label' => 'Disposition',
            'type' => \Elementor\Controls_Manager::SELECT,
            'options' => [
                true => 'Card',
                false => 'Line'
            ]
        ]);

        $this->end_controls_section();

        $this->start_controls_section('promo_filter', [
            'label' => 'Filters',
            'tab' => \Elementor\Controls_Manager::TAB_CONTENT
        ]);

        $this->add_control('limit', [
            'label' => 'Display number',
            'type' => \Elementor\Controls_Manager::NUMBER,
            'default' => 6
        ]);
        $this->add_control('referentiel', [
            'label' => 'Referentiel to display',
            'type' => \Elementor\Controls_Manager::TEXT
        ]);

        $this->end_controls_section();
    }

    protected function render()
    {
        $instance = $this->get_settings_for_display();

        require 'template.php';
    }
}
