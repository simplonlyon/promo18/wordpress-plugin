<?php

$args = [
    'post_type' => 'promo'
];

if ($instance['limit']) {
    $args['posts_per_page'] = $instance['limit'];
} else {
    $args['posts_per_page'] = -1;
}

if ($instance['referentiel']) {
    $args['meta_query'] = [
        [
            'key' => 'referentiel',
            'value' => $instance['referentiel']
        ]
    ];
}

$query = new WP_Query($args);

if (!$query->have_posts()) {

    echo "<p>Aucune promo disponible</p>";
    return;
}
if ($instance['is_flex']) {
    echo '<div class="promo-list cards">';
} else {
    echo '<div class="promo-list">';
}
while ($query->have_posts()) {
    $query->the_post();
    $referentiel = get_post_meta(get_the_ID(), 'referentiel', true);
    $start_date = get_post_meta(get_the_ID(), 'start_date', true);

?>
    <article>
        <h4><?= the_title() ?></h4>
        <p>Référentiel : <?= $referentiel ?></p>
        <?php if (!$instance['is_flex']) { ?>
            <p>Date de début : <?= $start_date ?></p>
        <?php } ?>
    </article>

<?php
}

echo '</div>';
