<?php
global $title;
?>

<h1><?= $title ?></h1>

<form method="get">
    <input type="hidden" name="page" value="simplon_admin_menu_postlist">
    <label for="search">Search : </label>
    <input type="text" name="search" id="search">
    <button>Go</button>
</form>

<?php
$search = $_GET['search'];

/**
 * On utilise l'objet WP_Query pour faire une requête SQL sur les posts/pages/autres
 * Cette requête peut recevoir beaucoup de paramètres listés ici : https://developer.wordpress.org/reference/classes/wp_query/#parameters
 */
$query = new WP_Query([
    'post_type' => 'any',
    's' => $search
]);

//On fait une boucle tant qu'il ya des posts dans les résultats
while ($query->have_posts()) {
    //On positionne le curseur sur le post actuel
    $query->the_post();
    //Ce qui nous permet d'utiliser les get pour récupérer et stocker les valeurs du post
    $author = get_the_author();
    

    //Ou bien les the_ pour afficher directement les valeurs du post en question
    echo '<div>';
    the_title();
    echo ' ' . $author;
    echo '</div>';
}
