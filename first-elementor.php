<?php
/**
 * Classe représentant un nouveau bloc élementor qu'il faudra charger dans le functions.php
 */
class FirstElementor extends \Elementor\Widget_Base
{

    /**
     * L'identifiant du bloc
     */
    public function get_name()
    {
        return 'first_elementor';
    }
    /**
     * Le titre du bloc qui sera affiché dans elementor
     */
    public function get_title()
    {
        return 'First Elementor';
    }
    /**
     * Cette fonction permet de générer le formulaire de configuration du bloc, si on a
     * des champs parametrables ou autre
     * Ici, on crée un champ de type texte qui s'appelle message
     */
    protected function _register_controls()
    {
        $this->start_controls_section('first_section', [
            'label' => 'Configure Widget',
            'tab' => \Elementor\Controls_Manager::TAB_CONTENT
        ]);

        $this->add_control('message', [
            'label' => 'Message',
            'type' => \Elementor\Controls_Manager::TEXT
        ]);

        $this->end_controls_section();
    }
    /**
     * La fonction d'affichage du bloc à proprepement parler, c'est là dedans qu'on
     * mettra le html/php de notre bloc et où on pourra utiliser les paramètres du 
     * bloc
     */
    protected function render()
    {
        $instance = $this->get_settings_for_display();

?>
<p><?= $instance['message'] ?></p>

<?php
    }
}
